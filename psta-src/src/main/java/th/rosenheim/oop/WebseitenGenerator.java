package th.rosenheim.oop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WebseitenGenerator {
    private List<Stadt> staedte = new ArrayList<>();

    public void addStadt(Stadt stadt) {
        staedte.add(stadt);
    }

    public String getNavigation() {
        String stadtString = String.join("| ", staedte.stream()
                .map(Stadt -> "<a href=\"" + Stadt.getURL() + "\">" + Stadt.name + "</a>")
                .toArray(String[]::new));
        return "<h1>Die Wetter-Website</h1><p>" + stadtString + "</p>";
    }

    public String generatePage(String navigation, String content) {
        return "<html><body>" + navigation + content + "</body></html>";
    }

    public void generate() {
        String navigation = getNavigation();
        try {
            File outputDir = new File("./output");
            if (!outputDir.exists()) {
                outputDir.mkdir();
            }

            FileWriter indexWriter = new FileWriter("./output/index.html");
            indexWriter.write(generatePage(navigation, ""));
            indexWriter.close();

            for (Stadt stadt : staedte) {
                FileWriter writer = new FileWriter("./output/" + stadt.getURL());
                writer.write(generatePage(navigation, stadt.getContent()));
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
