package th.rosenheim.oop;

import java.util.List;

public class Grossstadt extends Stadt {
    private List<String> stadtteile;

    public Grossstadt(String name, Wetter wetter, List<String> stadtteile) {
        super(name, wetter);
        this.stadtteile = stadtteile;
    }

    @Override
    public String getContent() {
        String stadtteileString = String.join(", ", stadtteile.subList(0, stadtteile.size() - 1))
                + " und " + stadtteile.get(stadtteile.size() - 1);
        return "<p>In " + name + " ist es " + wetter.getWetter() + ".</p>"
                + "<p> Dies trifft auch für " + stadtteileString + " zu.</p>";
    }

    @Override
    public String getURL() {
        return "wetter_grossstadt_" + formatNameForURL() + ".html";
    }
}
