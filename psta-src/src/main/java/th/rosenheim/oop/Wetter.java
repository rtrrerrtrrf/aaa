package th.rosenheim.oop;

// Enum representing different weather conditions
public enum Wetter {
    SONNIG("sonnig"),
    WOLKIG("wolkig"),
    REGNERISCH("regnerisch");

    private final String wetter;

    // Constructor for Wetter enum
    Wetter(String wetter) {
        this.wetter = wetter;
    }

    // Getter method to retrieve the weather condition
    public String getWetter() {
        return wetter;
    }
}
 